'use strict';
const nav = document.querySelector('#tabs .tabs-nav');
const templateTab = nav.querySelector('li');
const tabs = () => nav.querySelectorAll('li');
const content = document.querySelector('#tabs .tabs-content');
const articles = content.querySelectorAll('.tabs-content>[data-tab-title]');

const hiddenAllArticles = () => articles.forEach(item => item.classList.add('hidden'));
const showArticle = (eventElement) => {
	articles.forEach(item => {
		if (item.dataset.tabTitle.toUpperCase() === eventElement.innerText.toUpperCase()) {
			item.classList.remove('hidden');
		}
	});
};
const deactivateAllTabs = () => nav.querySelectorAll('li').forEach(item => item.classList.remove('ui-tabs-active'));
const activateTab = (tab) => tab.classList.add('ui-tabs-active');

const initTab = () => {
	hiddenAllArticles();
	nav.removeChild(tabs()[0]);

	articles.forEach(item => {
		let clone = templateTab.cloneNode(true);
		clone.children[0].classList.add(`${item.dataset.tabIcon}`);
		clone.children[0].innerText = `${item.dataset.tabTitle}`;

		clone.children[0].addEventListener('click', (event) => {
			event.preventDefault();
			deactivateAllTabs();
			activateTab(event.target.parentElement);
			hiddenAllArticles();
			showArticle(event.target);
		});
		nav.appendChild(clone);
	});

	activateTab(tabs()[0]);
	showArticle(tabs()[0].children[0]);
};

document.addEventListener('DOMContentLoaded', initTab);
