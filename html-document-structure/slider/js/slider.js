'use strict';
const slider = document.getElementsByClassName('slider')[0];
const slides = slider.getElementsByClassName('slide');
const buttons = slider.querySelectorAll('nav > a');
const nextButtonBlock = slider.querySelectorAll('a:nth-child(2n)');
const prevButtonBlock = slider.querySelectorAll('a:nth-child(2n-1)');
let currentSlide = 0;
const toggleSlide = (target) => {
	let finalDestination,
		targetSlide;
	switch (target) {
		case 'next':
			finalDestination = slides.length - 1;
			targetSlide = currentSlide + 1;
			break;
		case 'prev':
			finalDestination = 0;
			targetSlide = currentSlide - 1;
			break;
		case 'first':
			finalDestination = 0;
			targetSlide = 0;
			break;
		case 'last':
			finalDestination = slides.length - 1;
			targetSlide = slides.length - 1;
			break;
	}
	slides[currentSlide].classList.remove('slide-current');
	currentSlide = (currentSlide !== finalDestination) ? targetSlide : currentSlide;
	slides[currentSlide].classList.add('slide-current');

};
const setDisabledButton = () => {
	if (currentSlide === 0) {
		nextButtonBlock.forEach(item => item.classList.remove('disabled'));
		prevButtonBlock.forEach(item => item.classList.add('disabled'));
	} else if (currentSlide === slides.length - 1) {
		prevButtonBlock.forEach(item => item.classList.remove('disabled'));
		nextButtonBlock.forEach(item => item.classList.add('disabled'));
	} else {
		buttons.forEach(item => item.classList.remove('disabled'));
	}
};
const getSlide = (event) => {
	toggleSlide(event.target.dataset.action);
	setDisabledButton();
};


const init = () => {
	slides[0].classList.add('slide-current');
	setDisabledButton();
	for (let button of buttons) {
		button.addEventListener('click', getSlide);
	}
};

document.addEventListener('DOMContentLoaded', init);
