'use strict';
const list = document.querySelector('.todo-list');
const tasks = list.querySelectorAll('label');
const done = list.querySelector('.done');
const undone = list.querySelector('.undone');
const init = () => tasks.forEach(item => item.addEventListener('input', check));
const check = () => (event.target.checked) ?
	done.appendChild(event.target.parentElement) :
	undone.appendChild(event.target.parentElement);

document.addEventListener('DOMContentLoaded', init);
