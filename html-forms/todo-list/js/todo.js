'use strict';
const output = document.querySelector('output');
const todoListStyle = document.querySelector('.list-block').classList;
const taskItems = document.querySelectorAll('[type="checkbox"]');
const tasksItemAmount = taskItems.length;
const getTasksCompletedAmount = () => Array.from(taskItems).filter(item => item.checked).length;
const setTaskCompletedStyle = () => (tasksItemAmount === getTasksCompletedAmount()) ? todoListStyle.add('complete') : todoListStyle.remove('complete');
const setCompletedAmountInscription = () => output.value = `${getTasksCompletedAmount()} из ${tasksItemAmount}`;
const events = () => {
	setCompletedAmountInscription();
	setTaskCompletedStyle();
};

document.addEventListener('DOMContentLoaded', events);
for (let item of taskItems) {
	item.addEventListener('click', events);
}
