'use strict';

const content = document.querySelector('#content');
const loader = document.querySelector('#loader');
const from = () => Array.from(content.querySelectorAll('#from>option')).find(option => option.selected);
const to = () => Array.from(content.querySelectorAll('#to>option')).find(option => option.selected);
const selects = content.querySelectorAll('select');
let result = content.querySelector('#result');
const source = () => content.querySelector('#source').value;

const getCurrency = () => {
	return new Promise((resolve, reject) => {
		const xhr = new XMLHttpRequest();
		xhr.open('get', 'https://neto-api.herokuapp.com/currency');
		xhr.addEventListener('load', () => {
			result.value = source();
			loader.classList.toggle('hidden');
			content.classList.toggle('hidden');
			resolve(xhr.responseText);
		});
		xhr.addEventListener('error', () => reject(loader.innerText = 'Извините, сервер недоступен.'));
		xhr.send();
	});
};
loader.classList.toggle('hidden');
getCurrency()
	.then((jsonCurrency) => JSON.parse(jsonCurrency))
	.then(item =>
		item.map(({code, value}) =>
			selects.forEach(select => {
				select.innerHTML += `<option value="${value}">${code}</option>`;
			})));

let convert = () => source() * from().value / to().value;
const setResult = () => result.value = convert().toFixed(2);

for (let item of content.querySelectorAll('*:nth-child(-n+4)[id]')) {
	item.addEventListener('input', setResult);
}