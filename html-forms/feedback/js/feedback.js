'use strict';
const contentForm = document.querySelector('.contentform ');
const inputs = contentForm.querySelectorAll('.form-group>*:nth-child(3)');
const zipInputs = contentForm.querySelector('input[name="zip"]');
const sendButton = contentForm.querySelector('.button-contact[type="submit"]');
const outputForm = document.querySelector('#output');
const outputs = outputForm.querySelectorAll('output');
const editButton = outputForm.querySelector('.button-contact');
const toggleForms = () => {
	contentForm.classList.toggle('hidden');
	outputForm.classList.toggle('hidden');
};
const fillOutputForm = () => {
	for (let output of outputs) {
		output.value = contentForm.querySelector(`[name = ${output.id}]`).value;
	}
};
const isFilled = () => Array.from(inputs).every(item => item.value);
const activateSendButton = () => sendButton.disabled = false;
const inspect = () => {
	if (isFilled()) {
		activateSendButton();
	}
};
const clickButton = (evt) => {
	evt.preventDefault();
	toggleForms();
	fillOutputForm();
};
const init = () => {
	zipInputs.type = 'number';
	for (let input of inputs) {
		input.addEventListener('input', inspect);
	}
	sendButton.addEventListener('click', clickButton);
	editButton.addEventListener('click', toggleForms);
};
init();