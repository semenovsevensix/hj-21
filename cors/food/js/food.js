'use strict';
const randName = () => `f${Date.now().toString(36)}`;
const dataPic = document.querySelector('[data-pic]');
const dataTitle = document.querySelector('[data-title]');
const dataIngredients = document.querySelector('[data-ingredients]');
const dataRating = document.querySelector('[data-rating]');
const dataStar = document.querySelector('[data-star]');
const dataVotes = document.querySelector('[data-votes]');
const dataConsumers = document.querySelector('[data-consumers]');

const parse = ([{id, title, pic, ingredients}, {rating, votes}, {consumers, total}]) => {
	dataPic.style.backgroundImage = `url(${pic})`;
	dataTitle.appendChild(document.createTextNode(title));
	dataIngredients.appendChild(document.createTextNode(ingredients.join(',')));
	dataRating.appendChild(document.createTextNode(rating.toFixed(2)));
	dataStar.style.width = `${rating / (10 / 100)}%`;
	dataVotes.appendChild(document.createTextNode(`${votes} оценок`));
	dataConsumers.appendChild(consumers.reduce((f, {name, pic}, index) => {
		let elem = document.createElement('img');
		elem.src = pic;
		elem.title = title;
		f.appendChild(elem);
		if (index === 3) {
			let elem = document.createElement('span');
			elem.appendChild(document.createTextNode(`(+${total})`));
			f.appendChild(elem);
		}
		return f;
	}, document.createDocumentFragment()));

};
const loadData = (url) => {
	const functionName = randName();
	return new Promise((done, fail) => {
		window[functionName] = done;
		const script = document.createElement('script');
		script.src = `${url}?callback=${functionName}`;
		document.body.appendChild(script);
	});
};

Promise.all([
	loadData('https://neto-api.herokuapp.com/food/42'),
	loadData('https://neto-api.herokuapp.com/food/42/rating'),
	loadData('https://neto-api.herokuapp.com/food/42/consumers')
]).then(parse);