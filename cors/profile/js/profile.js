'use strict';
const content = document.getElementsByClassName('content')[0];
const dataName = document.querySelector(`[data-name`);
const dataDescription = document.querySelector(`[data-description`);
const dataPic = document.querySelector(`[data-pic`);
const dataPosition = document.querySelector(`[data-position`);
const dataTechnologies = document.querySelector(`[data-technologies`);
const randName = () => `f${Date.now().toString(36)}`;
const functionName = randName();

const parse = ({id, name, description, pic, position}) => {
	dataName.appendChild(document.createTextNode(name));
	dataDescription.appendChild(document.createTextNode(description));
	dataPic.src = pic;
	dataPosition.appendChild(document.createTextNode(position));
	return id;
};

const parseTechnologies = (data) => {
	if (Array.isArray(data)) {
		for (let item of data) {
			let span = document.createElement('span');
			span.classList.add('devicons');
			span.classList.add(`devicons-${item}`);
			dataTechnologies.appendChild(span);
		}
	}
	content.style = 'display: initial';
};

const loadData = (url) => {
	return new Promise((done, fail) => {
		window[functionName] = done;
		const script = document.createElement('script');
		script.src = url;
		document.body.appendChild(script);
	});
};

loadData(`https://neto-api.herokuapp.com/profile/me?callback=${functionName}`)
	.then(parse)
	.then((id) => loadData(`https://neto-api.herokuapp.com/profile/${id}/technologies?callback=${functionName}`)
		.then(parseTechnologies)
		.catch(e => console.log(e)))
	.catch(e => console.log(e));
