'use strict';
const randName = () => `f${Date.now().toString(36)}`;
const parse = (data) => {
	for (let key of Object.keys(data)) {
		let elem = document.querySelector(`[data-${key}`);
		(key === 'wallpaper' || key === 'pic') ?
			elem.src = data[key] :
			elem.appendChild(document.createTextNode(data[key]));
	}
};

const loadData = (url) => {
	const functionName = randName();
	return new Promise((done, fail) => {
		window[functionName] = done;
		const script = document.createElement('script');
		script.src = `${url}?callback=${functionName}`;
		document.body.appendChild(script);
	});
};

loadData('https://neto-api.herokuapp.com/twitter/jsonp')
	.then(parse)
	.catch(e => console.log(e));
