'use strict';
const longPoling = document.getElementsByClassName('long-pooling')[0].children;
let longPast = 0;
const getLongData = () => {
	const xhrL = new XMLHttpRequest();
	xhrL.open('GET', 'https://neto-api.herokuapp.com/comet/long-pooling');
	xhrL.send();
	xhrL.addEventListener('load', () => {
		let res = xhrL.responseText.trim();
		if (res !== longPast || res >= longPoling.length) {
			longPoling[res].classList.add('flip-it');
			longPoling[longPast].classList.remove('flip-it');
			longPast = res;
		}
		getLongData();
	});
};
getLongData();