'use strict';
const poling = document.getElementsByClassName('pooling')[0].children;
let past = 0;
const getData = ()=>{
	const xhr = new XMLHttpRequest();
	xhr.open('GET','https://neto-api.herokuapp.com/comet/pooling');
	xhr.send();
	xhr.addEventListener('load',()=>{
		if(xhr.responseText !== past || xhr.responseText >= poling.length){
		poling[xhr.responseText].classList.add('flip-it');
		poling[past].classList.remove('flip-it')
			past = xhr.responseText;
		}

	});
};
getData()
setInterval(getData,5000);

