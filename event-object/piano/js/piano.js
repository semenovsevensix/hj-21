'use strict';
const piano = document.getElementsByClassName('set')[0];
const keys = document.getElementsByClassName('set')[0].children;
const tonality = [
	'lower',
	'middle',
	'higher'
];
const toneNow = () => (event && event.altKey)?tonality[2]:(event && event.shiftKey)?tonality[0]:tonality[1];
const sounds = [
	() => `https://netology-code.github.io/hj-homeworks/event-object/piano/sounds/${toneNow()}/first.mp3`,
	() => `https://netology-code.github.io/hj-homeworks/event-object/piano/sounds/${toneNow()}/second.mp3`,
	() => `https://netology-code.github.io/hj-homeworks/event-object/piano/sounds/${toneNow()}/third.mp3`,
	() => `https://netology-code.github.io/hj-homeworks/event-object/piano/sounds/${toneNow()}/fourth.mp3`,
	() => `https://netology-code.github.io/hj-homeworks/event-object/piano/sounds/${toneNow()}/fifth.mp3`
];
let setClass = function(){
	let tone = toneNow();
	if(!piano.classList.contains(tone)){
		piano.classList.remove(piano.classList[1]);
		piano.classList.add(tone)
	}
};

let setSound = function(){
	let i = 0;
	for( let key of keys){
		key.children[0].src = sounds[i]();
		i++;
	}
};
let init = function(){
	setClass();
	setSound();
};


let keyPlay = () => {
	init();
	event.currentTarget.children[0].play();
};

for(let key of keys){
	key.addEventListener('click',keyPlay);

}
document.addEventListener('keydown',setClass);
document.addEventListener('keyup',setClass);


