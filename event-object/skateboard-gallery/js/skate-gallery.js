'use strict';
const photos = document.getElementsByTagName('a');
const view = document.getElementsByClassName('gallery-view')[0];
let currentPhoto = function () {
	for (let photo of photos) {
		if (photo.classList.contains('gallery-current')) {
			return photo;
		}
	}
};
const getHref = function (event) {
	event.preventDefault();
	view.src = event.currentTarget.href;
	currentPhoto().classList.remove('gallery-current');
	event.currentTarget.classList.add('gallery-current');
};
for (let photo of photos) {
	photo.addEventListener('click', getHref);
}