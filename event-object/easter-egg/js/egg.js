'use strict';
const panel = document.getElementsByTagName('nav')[0];
const keyCode = ['KeyY', 'KeyT', 'KeyN', 'KeyJ', 'KeyK', 'KeyJ', 'KeyU', 'KeyB', 'KeyZ'];
const secret = document.getElementsByClassName('secret')[0];
let counter = 0;
let make = function () {
	if (event.altKey && event.ctrlKey && (event.code === 'KeyT')) {
		panel.classList.toggle('visible');
	}
	if (event.code === keyCode[counter]) {
		counter++;
		if (counter === keyCode.length) {
			secret.classList.add('visible');
		}
	} else {
		counter = 0;
	}
};
document.addEventListener('keydown', make);