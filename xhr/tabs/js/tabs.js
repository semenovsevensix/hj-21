'use strict';
const tabs = document.querySelectorAll('nav a');
const content = document.querySelector('#content');
const preloader = document.querySelector('#preloader');
const tabActive = () => document.querySelector('nav a.active');
const xhr = new XMLHttpRequest();
const getTab = function () {
	xhr.open('get', tabActive().href);
	xhr.send();
};
preloader.classList.remove('hidden');
getTab();

function setTab() {
	if (!preloader.classList.contains('hidden')) {
		preloader.classList.add('hidden');
	}
	content.innerHTML = xhr.responseText;
}

function switchTab(e) {
	e.preventDefault();
	if (!e.target.classList.contains('active')) {
		content.innerHTML = '';
		preloader.classList.remove('hidden');
		tabActive().classList.remove('active');
		e.target.classList.add('active');
		getTab();
	}
}

for (let tab of tabs) {
	tab.addEventListener('click', switchTab);
}
xhr.addEventListener('load', setTab);
