'use strict';
const content = document.querySelector('#content');
const xhr = new XMLHttpRequest;
xhr.open('get','https://neto-api.herokuapp.com/book/');
xhr.send();
let parse = () => JSON.parse(xhr.responseText)
	.map(({title:title,author:{name:author},cover:{small:img},info,price})=>
		content.innerHTML += `<li data-title = "${title}" data-author = "${author}" data-info = "${info}" data-price = "${price}"><img src="${img}"></li>`)
content.innerHTML='';
xhr.addEventListener('load',parse);