'use strict';
const directory = 'i';
const arrImg = [
	'airmax-jump.png',
	'airmax-on-foot.png',
	'airmax-playground.png',
	'airmax-top-view.png',
	'airmax.png'
];
const slider = document.getElementById('slider');
let i = 0;

function slide() {
	slider.src = `./${directory}/${arrImg[i]}`;
	 (i === arrImg.length-1)? i=0: i++;
}

document.addEventListener("DOMContentLoaded" ,function() {
	setInterval(slide,5000);

});

