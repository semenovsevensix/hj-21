'use strict';
const images = [
	'i/breuer-building.jpg',
	'i/guggenheim-museum.jpg',
	'i/headquarters.jpg',
	'i/IAC.jpg',
	'i/new-museum.jpg'
];
const prev = document.getElementById('prevPhoto');
const next = document.getElementById('nextPhoto');
const current = document.getElementById('currentPhoto');
let i = 0;
current.src = images[0];

function prevPhoto(){
	 (i === 0)?i =images.length-1:i--;
	current.src = images[i]
}
function nextPhoto(){
	 (i === images.length-1) ? i = 0 : i++;
	current.src = images[i]
}
prev.onclick = prevPhoto;
next.onclick = nextPhoto;

