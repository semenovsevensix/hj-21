'use strict';
const contacts = JSON.parse(loadContacts());
const contactsListElem = document.querySelector('.contacts-list');
contactsListElem.innerHTML = '';
let tmp = '';
for (let contact of contacts) {
	tmp += `<li data-email="${contact.email}" data-phone="${contact.phone}"><strong>${contact.name}</strong></li>`;
}
contactsListElem.innerHTML = tmp;