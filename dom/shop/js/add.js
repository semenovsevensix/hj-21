'use strict';
const buttons = document.querySelectorAll('button.add');
const count = document.querySelector('#cart-count');
const totalPrice = document.querySelector('#cart-total-price');
let countValue = 0;
let totalPriseValue = 0;

for (let button of buttons) {
	button.addEventListener('click', function () {
		count.innerText = ++countValue;
		totalPrice.innerText = getPriceFormatted(totalPriseValue += parseInt(event.currentTarget.dataset.price));
	});
}