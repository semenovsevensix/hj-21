'use strict';
const chat = document.getElementsByClassName('chat')[0];
const messageBox = chat.getElementsByClassName('message-box')[0];
const messageInput = chat.getElementsByClassName('message-input');
const submitBtn = chat.getElementsByClassName('message-submit')[0];
const messageContent = () => chat.getElementsByClassName('messages-content');
const templatesBox = chat.getElementsByClassName('messages-templates')[0];
const chatStatus = chat.getElementsByClassName('chat-status')[0];
const messageTemplates = templatesBox.getElementsByClassName('message');
const message = {
	'loading': messageTemplates[0],
	'message': messageTemplates[1],
	'message-personal': messageTemplates[2],
	'message-status': messageTemplates[3],
};
const getTime = (stamp) => {
	const date = new Date(stamp);
	let hours = date.getHours();
	let minutes = date.getMinutes();
	return `${(hours < 10) ? '0' + hours : hours}:${(minutes < 10) ? '0' + minutes : minutes}`;
};
const postMessage = ({type, data}) => {
	let thisLoading;
	if (data === '...') {
		thisLoading = messageContent()[0].appendChild(message['loading']);
	}
	if (type === 'message' || type === 'message-personal' || type === 'message-status') {
		if (thisLoading) {
			messageContent().removeChild(messageContent().lastChild);
		}
		let msg = message[type].cloneNode(true);

		msg.querySelector('.message-text').innerText = data;
		if (type !== 'message-status') {
			msg.querySelector('.timestamp').innerText = getTime(Date.now());
		}
		messageContent()[0].appendChild(msg);
		messageContent()[0].scrollTop = messageContent()[0].scrollHeight;
	}
};
const userToggleState = () => {
	let flag = connection.readyState === 1;
	submitBtn.disabled = !flag;
	chatStatus.innerText = (flag) ? chatStatus.dataset.online : chatStatus.dataset.offline;
	postMessage({
		type: 'message-status',
		data: (flag) ? 'Пользователь появился в сети' : 'Пользователь не в сети',
	});
};
const userMessageSend = (event) => {
	event.preventDefault();
	let config = {
		type: 'message-personal',
		data: messageInput[0].value,
	};
	if (config.data !== '') { //Не даю отправить пустое сообщение, при пустом сообщении,в окне чата, появляется горизонтальная прокрутка
		connection.send(JSON.stringify(config));
		messageInput[0].value = '';
		postMessage(config);
	}
};
const postError = () => {
	postMessage({
		type: 'message-status',
		data: 'Чат времмено недоступен :('
	});
};

const connection = new WebSocket('wss://neto-api.herokuapp.com/chat');

connection.addEventListener('open', userToggleState);
connection.addEventListener('close', userToggleState);
connection.addEventListener('message', postMessage);
connection.addEventListener('error', postError);
messageBox.addEventListener('submit', userMessageSend);

//Добавил прокрутку,а то тестить не удобно:

messageContent()[0].style.overflowY = 'auto';



