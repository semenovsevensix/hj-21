'use strict';
const player = document.getElementsByClassName('mediaplayer')[0];
const playlist = [
	'./mp3/LA Chill Tour.mp3',
	'./mp3/This is it band.mp3',
	'./mp3/LA Fusion Jam.mp3'
];
let trackNum = 0;
let trackTitle = () => (playlist[trackNum].split('\\').pop().split('/').pop().split('.'))[0];
let track = player.firstElementChild;

document.onclick = function () {
	for (let item of document.getElementsByClassName('fa')) {

		item.parentElement.onclick = function () {
			if (item.parentElement.classList.contains('playstate')) {
				player.classList.toggle('play');
				if (document.getElementsByClassName('play')[0] !== undefined) {
					track.play();
				} else {
					track.pause();
				}
			} else if (item.parentElement.classList.contains('stop')) {
				player.classList.remove('play');
				track.pause();
				track.currentTime = 0;
			} else if (item.parentElement.classList.contains('back')) {
				trackNum = (trackNum === 0) ? playlist.length - 1 : trackNum - 1;
				track.src = playlist[trackNum];
				document.getElementsByClassName('title')[0].title = trackTitle();
				if(player.classList.contains('play')){
					track.play()
				}
			} else if (item.parentElement.classList.contains('next')) {
				trackNum = (trackNum === playlist.length - 1) ? 0 : trackNum + 1;
				track.src = playlist[trackNum];
				document.getElementsByClassName('title')[0].title = trackTitle();
				if(player.classList.contains('play')){
					track.play()
				}
			}
		};

	}
};