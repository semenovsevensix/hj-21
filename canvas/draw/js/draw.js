'use strict';
const canvas = document.getElementById('draw');
const ctx = canvas.getContext('2d');
const HSL_SATURATION = '100%';
const HSL_LIGHTNESS = '50%';
let hslHue = 0;
let brushRadius = 50;
let brushRadiusDecrement = true;

const bodyWidth = () => window.innerWidth;
const bodyHeight = () => window.innerHeight;
const resizeCanvas = () => {
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	canvas.setAttribute('width', bodyWidth());
	canvas.setAttribute('height', bodyHeight());
};
const getHslHue = (decrement) => {
	const HUE_MAX = 359;
	const HUE_MIN = 0;
	if (decrement) {
		(hslHue > HUE_MIN) ? hslHue -= 1 : hslHue = HUE_MAX;

	} else {
		(hslHue < HUE_MAX) ? hslHue += 1 : hslHue = HUE_MIN;
	}
	return hslHue;
};

const getBrushRadius = () => {

	if (brushRadiusDecrement) {
		brushRadius -= 0.5;
		if (brushRadius === 2.5) {
			brushRadiusDecrement = false;
		}
	} else {
		brushRadius += 0.5;
		if (brushRadius === 50) {
			brushRadiusDecrement = true;
		}
	}
	return brushRadius;
};

const init = () => {
	ctx.lineJoin = 'round';
	ctx.lineCap = 'round';
	resizeCanvas();
};
let resizeTimeout;
const resizeThrottler = () => {
	if (!resizeTimeout) {
		resizeTimeout = setTimeout(function () {
			resizeTimeout = null;
			resizeCanvas();
		}, 66);
	}
};

const draw = (event) => {
	ctx.fillStyle = `hsl(${getHslHue(event.shiftKey)},${HSL_SATURATION},${HSL_LIGHTNESS})`;
	ctx.beginPath();
	ctx.arc(event.x, event.y, getBrushRadius(), 0, 2 * Math.PI);
	ctx.fill();
};
document.addEventListener('DOMContentLoaded', init, false);
window.addEventListener('resize', resizeThrottler, false);
canvas.addEventListener('mousedown', (event) => {

	if (event.buttons === 1) {
		canvas.addEventListener('mousemove', draw);
	}
});
canvas.addEventListener('mouseup', () => canvas.removeEventListener('mousemove', draw));
canvas.addEventListener('dblclick', () => ctx.clearRect(0, 0, canvas.width, canvas.height));
